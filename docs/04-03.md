---
id: 04-03
title: "Unit 4 Chapter 3: Development Image"
sidebar_label: "Chapter 3: Development Image"
---

## Building the Docker Image
* In the LXTerminal window, enter the following commands:
```
cd
cd rubyonracetracks
cd docker-debian-stretch-build
cd dev
sh build.sh
```
* Your development Docker image will be ready in a few minutes.
