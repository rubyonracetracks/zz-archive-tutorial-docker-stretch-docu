---
id: 01-02
title: "Unit 1 Chapter 2: Installing Docker"
sidebar_label: "Chapter 2: Installing Docker"
---

* NOTE: If you are using a Linux distro that is NOT based on Debian Stable, you'll have to perform the tasks in this chapter manually.
* Open a new terminal window.  In SparkyLinux, go to Menu -> System Tools -> LXTerminal.
* Install Git (the standard version control system) and Python (needed for blocking ads, malware, news, etc.).  In the terminal window, enter the following command:
```
sudo apt-get update; sudo apt-get install -y git
```
* To install Docker, enter the following commands:
```
mkdir rubyonracetracks
cd rubyonracetracks
git clone https://gitlab.com/rubyonracetracks/docker-debian-stretch-install.git
cd docker-debian-stretch-install
sh main.sh
```
* Docker will be installed in a few minutes.  In addition, other development software that you will need later is also installed, including KeePassX (for generating, encrypting, and storing passwords), Geany (code editor), SearchMonkey (search engine for local files), SQLite database browser (for viewing the SQLite database), and PG Admin (for viewing the PostgreSQL database).
* Don't worry if the Docker installation process results in the message "Are you trying to connect to a TLS-enabled daemon without TLS?"  This is normal.
* After Docker has been installed, remove the Docker installation source code now that you're finished with it.  Enter the following commands:
```
cd
cd rubyonracetracks
rm -rf docker-debian-stretch-install
```
* Reboot Linux.
