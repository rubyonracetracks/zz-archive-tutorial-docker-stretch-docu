---
id: 03-00
title: Unit 3 Introduction
---

In this unit, you will use the rails-general Docker image and learn the Ruby on Racetracks way to view an app in your browser and its SQLite or PostgreSQL database.
