---
id: 04-01
title: "Unit 4 Chapter 1: Getting Started"
sidebar_label: "Chapter 1: Getting Started"
---

In Unit 4, you will build the Docker images provided by the Ruby on Racetracks Docker infrastructure.

## 32-Bit Images
Please note that I no longer provide 32-bit Docker images.

## Docker Hub
I rely on Docker Hub's automated build system to create and save Docker images.  This is fast and convenient.

## Local Environment
* Although I rely on Docker Hub to create Docker images, it's a good idea to run the build process on a local machine to make sure that it works properly.  If things don't pan out, the resulting bad Docker image is not saved in Docker Hub.
* In the next few chapters, you will build Docker images in your local environment.
* For building new Docker images, do NOT rely on the same system where you are working on other projects with the assistance of Docker.
* Building Docker images requires a clean Docker environment to avoid interference from old Docker images.  This is why you will be asked to run a script to delete all Docker images and containers.
* Your options for an isolated environment are the following:
  * Use a different physical machine.
  * Use a different OS on the same physical machine with a dual-boot or multi-boot setup.
  * Use [VirtualBox](http://www.virtualboxtutorial.com/) to provide an isolated development environment.

 
